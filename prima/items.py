# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class PrimaItem(scrapy.Item):
    # define the fields for your item here like:
    url = scrapy.Field()
    name = scrapy.Field()
    price =  scrapy.Field()
    delivery_term = scrapy.Field()
    category = scrapy.Field()
    id_ware = scrapy.Field()
    presense = scrapy.Field()
    parsing_time = scrapy.Field() 


