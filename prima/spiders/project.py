import time
import scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.spiders import Rule, CrawlSpider
from scrapy.linkextractors import LinkExtractor as sle
from prima.items import PrimaItem



class FleurSpider(CrawlSpider):
	name = 'fleur'
	allowed_domains = ['fleur.ua']
	start_urls = [
		#'http://fleur.ua/parfyumeriya/mujskaya-parfyumeriya.html',
		#'http://fleur.ua/parfyumeriya/jenskaya-parfyumeriya.html',
		#'http://fleur.ua/parfyumeriya/parfyumernie-nabory.html',
		'http://fleur.ua/parfyumeriya/nishevaja-parfjumerija.html',
	]

	def parse(self, response):
		links = response.css('div.category-products a::attr(href)').extract()
		#links = response.xath('//div/a/@href').extract()
		# del twins
		links = list(set(i for i in links if 'http://fleur.ua' in i))
		padd = [i for i in links if '.html?p' in i]
		padd.sort()
		norm = [i for i in links if '.html?p' not in i]
		for i in norm:
			next_page = response.urljoin(i)
			yield scrapy.Request(next_page, callback=self.parse_0)
		for link in padd:
			if link > response.url:
				next_page = response.urljoin(link)
				yield scrapy.Request(next_page, callback=self.parse)
	
	def parse_0(self, response):
		#item = PrimaItem()
		item = dict()
		item['url'] = response.url
		item['name'] = response.css('h1.product-name::text').extract()
		item['parsing_time'] = time.time()
		item['delivery_term'] = 'Поставка ожидается'
		item['category'] = response.xpath('//span[@class="info-row bold"]/text()').extract()[0]
		id_ware = response.css('span.code::text')
		if id_ware:	
			for i in id_ware.extract():
				item['id_ware'] = i.split()[-1]
		else:
			item['id_ware'] = 'no id'
		
		presense1 = response.xpath('//p[@class="availability in-stock"]/span/text()').extract()
		if presense1:
		    item['price'] =  response.css('span.price::text').extract()[0].replace('\xa0', '')
		    item['presense'] = "Да"
		elif not presense1:
		    item['presense'] = "Ожидается"
		    item['price'] =  "По предзаказу получите 3% скидки."
		with open('base.json', 'w') as f:
			json.dump(item, f) 
		#return item

a = FleurSpider()
process = CrawlerProcess()
process.crawl(a)#FleurSpider)
process.start()

