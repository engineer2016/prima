Паук для магазина www.fleur.ua.

Запускать как по стандарту (тип файлов json, xml..):

scrapy crawl fleur -o smt.csv -t csv 

Пакеты для работы:

attrs==16.2.0
automaton==1.5.0
Babel==2.3.4
cachetools==2.0.0
cffi==1.8.3
constantly==15.1.0
contextlib2==0.5.4
cryptography==1.5.3
cssselect==1.0.0
debtcollector==1.9.0
decorator==4.0.10
fasteners==0.14.1
flake8==3.0.4
futurist==0.19.0
idna==2.1
incremental==16.10.1
iso8601==0.1.11
jsonschema==2.5.1
lxml==3.6.4
mccabe==0.5.2
monotonic==1.2
msgpack-python==0.4.8
netaddr==0.7.18
netifaces==0.10.5
networkx==1.11
oslo.i18n==3.10.0
oslo.serialization==2.14.0
oslo.utils==3.18.0
parsel==1.0.3
pbr==1.10.0
prettytable==0.7.2
pyasn1==0.1.9
pyasn1-modules==0.0.8
pycodestyle==2.0.0
pycparser==2.17
PyDispatcher==2.0.5
pyflakes==1.2.3
pyOpenSSL==16.2.0
pyparsing==2.1.10
pytz==2016.7
queuelib==1.4.2
requests==2.11.1
retrying==1.3.3
Scrapy==1.2.1
service-identity==16.0.0
six==1.10.0
stevedore==1.18.0
taskflow==2.7.0
Twisted==16.5.0
w3lib==1.15.0
wrapt==1.10.8
zope.interface==4.3.2
